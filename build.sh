#!/usr/bin/env bash
SCRIPT_DIR=`dirname "$0"`
if command -v nix-shell &> /dev/null; then
	# If in NixOS use shell.nix
	nix-shell --pure $SCRIPT_DIR/shell.nix --run "cd $SCRIPT_DIR && hugo"
else
	# Otherwise use global hugo
	DIR=$PWD
	cd $SCRIPT_DIR
	hugo
	cd $DIR
fi
