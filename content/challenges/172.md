---
japanese:
  - - - pos: noun
        text: スピーカー
      - dictionary: ~
        text: "　"
      - pos: verb
        text: キズついた
        dictionary: 傷つく
      - pos: noun
        text:
          - 時: とき
      - dictionary: ~
        text: "　"
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 思: おも
          - い
          - 出: だ
          - した
        dictionary: 思い出す
  - - - pos: adverb
        text:
          - 深: ふか
          - く
      - pos: verb
        text:
          - 響: ひび
          - く
      - dictionary: ~
        text: "　"
      - pos: adjective
        text:
          - 力: ちから
          - 強: づよ
          - い
      - pos: noun
        text: メロディ
        dictionary: メロディー
      - pos: particle
        text: を
  - - - pos: noun
        text:
          - 願: ねが
          - い
      - dictionary: ~
        text: "　"
      - pos: adverb
        text: いつでも
        dictionary: 何時でも
      - pos: noun
        text: ここ
        dictionary: 此処
      - pos: particle
        text: で
  - - - pos: noun
        text:
          - 遠: とお
          - く
      - pos: particle
        text: へ
      - pos: particle
        text: と
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 君: きみ
      - pos: particle
        text: へ
      - pos: particle
        text: と
      - pos: verb
        text:
          - 届: とど
          - け
        dictionary: 届く
song:
  japanese: サウンド
youtube: y9uxB_ddC9c
suggester: mochamoko
date: 2024-09-03
---