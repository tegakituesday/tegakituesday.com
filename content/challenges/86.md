---
japanese:
  - - - pos: noun
        text:
          - 結: けっ
          - 界: かい
      - pos: particle
        text: を
      - pos: verb
        text:
          - 張: は
          - れ
        dictionary: 張る
      - pos: noun
        text:
          - スーパー
          - 銭: せん
          - 湯: とう
      - pos: particle
        text: で
  - - - pos: verb
        text:
          - 疲: つか
          - れ
        dictionary: 疲れる
      - pos: verb
        text:
          - 癒: い
          - える
      - pos: particle
        text: まで
        dictionary: 迄
      - pos: verb
        text:
          - 浸: つ
          - かれ
        dictionary: 浸かる
      - pos: noun
        text:
          - 心: こころ
      - pos: particle
        text: まで
        dictionary: 迄
      - pos: verb
        text:
          - 届: とど
          - け
        dictionary: 届く
  - - - pos: noun
        text:
          - 結: けっ
          - 界: かい
      - pos: particle
        text: を
      - pos: verb
        text:
          - 張: は
          - れ
      - pos: noun
        text:
          - 地: ち
          - 中: ちゅう
          - 海: かい
      - pos: noun
        text:
          - 式: しき
      - pos: noun
        text:
          - 食: しょく
          - 事: じ
      - pos: particle
        text: で
  - - - pos: verb
        text:
          - 喰: く
          - らえ
        dictionary: 食らう
      - pos: noun
        text:
          - 八: はち
          - 分: ぶん
          - 目: め
      - pos: particle
        text: で
      - pos: verb
        text:
          - 止: と
          - まれ
        dictionary: 止まる
      - pos: noun
        text:
          - 未: み
          - 来: らい
      - pos: particle
        text: まで
        dictionary: 迄
      - pos: verb
        text:
          - 続: つづ
          - け
        dictionary: 続ける
song:
  japanese: "[結]{けっ}[界]{かい}"
youtube: 1v3PCeHBuFg
suggester: 猛火妹紅（MochaMoko）
date: 2022-11-22
---
