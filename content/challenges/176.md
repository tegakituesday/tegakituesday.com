---
japanese:
  - - - pos: adjective
        text:
          - 青: あお
          - い
      - pos: noun
        text: ライト
      - pos: particle
        text: が
      - pos: verb
        text:
          - 照: て
          - らした
        dictionary: 照らす
      - pos: noun
        text:
          - 隙: すき
          - 間: ま
  - - - pos: noun
        text:
          - 膝: ひざ
      - pos: particle
        text: を
      - pos: verb
        text:
          - 抱: かか
          - えて
        dictionary: 抱える
      - pos: verb
        text:
          - 座: すわ
          - り
          - 込: こ
          - んでいた
        dictionary: 座り込む
  - - - pos: noun
        text:
          - 画: が
          - 面: めん
          - 越: ご
          - し
      - pos: particle
        text: の
      - pos: noun
        text: あなた
        dictionary: 貴方-1
      - pos: particle
        text: は
    - - pos: noun
        text: わたし
        dictionary: 私
      - pos: particle
        text: に
      - pos: verb
        text: 似ている
        dictionary: 似る
      - pos: phrase
        text: んだ
        dictionary: のです
song:
  japanese: "[合]{号}[成]{せい}するミライ"
  english: Synthesize You
youtube: IGu9FL_fQnA
suggester: mochamoko
date: 2024-10-22
---