---
japanese:
  - - - pos: noun
        text:
          - 噂: うわさ
      - pos: particle
        text: の
      - pos: adjective
        text: あの
        dictionary: 彼の
      - pos: noun
        text:
          - 子: こ
      - pos: particle
        text: の
      - pos: noun
        text:
          - 銀: ぎん
      - pos: particle
        text: の
      - pos: noun
        text:
          - 飴: あめ
  - - - pos: adverb
        text:
          - 漫: そぞ
          - ろ
      - pos: verb
        text:
          - 倣: なら
          - う
      - pos: particle
        text: は
      - pos: noun
        text:
          - 義: ぎ
          - 務: む
      - pos: particle
        text: か
      - pos: phrase
        text: はたまた
        dictionary: 将又
      - pos: noun
        text:
          - 自: じ
          - 由: ゆう
      - pos: particle
        text: か
  - - - pos: noun
        text:
          - 異: い
          - 様: よう
      - pos: particle
        text: に
      - pos: verb
        text:
          - 錆: さ
          - びついた
        dictionary: 錆つく
      - pos: noun
        text:
          - 脳: のう
      - pos: noun
        text:
          - 回: かい
          - 路: ろ
  - - - pos: phrase
        text: されど
        dictionary: 然れど
      - pos: noun
        text:
          - 何: なに
      - pos: noun
        text:
          - 一: ひと
          - つ
      - pos: particle
        text: とて
      - pos: noun
        text:
          - 問: もん
          - 題: だい
      - pos: particle
        text: は
      - pos: adjective
        text:
          - 無: な
          - い
      - pos: noun
        text:
          - 様: よう
        dictionary: 様-2
english:
  - Buzzy is that kid’s silver candy.
  - Is it compulsory to imitate them for no reason, or is it voluntary?
  - Their brain circuits are abnormally rusty.
  - But it appears nothing is wrong with them at all.
song:
  japanese: プシ
  english: Psi
youtube: efXqozK4A40
translation:
  author: Tackmyn Y.
  site:
    name: Vocaloid Lyrics Wiki
    link: https://vocaloidlyrics.fandom.com/wiki/プシ_(Psi)
date: 2024-06-11
---