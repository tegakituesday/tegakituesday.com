---
japanese:
  - - - pos: adjective
        text:
          - 惨: さん
          - 憺: たん
      - pos: adjective
        text: たる
      - pos: noun
        text:
          - 結: けつ
          - 末: まつ
      - pos: particle
        text: は
      - pos: noun
        text:
          - 美: うつく
          - しさ
      - pos: particle
        text: を
      - pos: verb
        text:
          - 纏: まと
          - う
      - pos: particle
        text: ほど
  - - - pos: adverb
        text:
          - 限: かぎ
          - りなく
        dictionary: 限り無く
      - dictionary: ~
        text: 、
      - pos: noun
        text:
          - 体: たい
          - 温: おん
      - pos: particle
        text: に
      - pos: adjective
        text:
          - 近: ちか
          - い
  - - - dictionary: ~
        text: 「
      - pos: noun
        text:
          - 赤: あか
      - dictionary: ~
        text: 」
      - pos: particle
        text: に
      - pos: verb
        text:
          - 彩: いろど
          - られていた
        dictionary: 彩る
  - - - pos: adjective
        text:
          - 散: さん
          - 漫: まん
      - pos: particle
        text: な
      - pos: noun
        text:
          - 視: し
          - 界: かい
      - pos: particle
        text: でも
      - pos: noun
        text:
          - 美: うつく
          - しさ
      - pos: particle
        text: が
      - pos: verb
        text: わかる
        dictionary: 分かる
      - pos: particle
        text: ほど
        dictionary: 程
  - - - pos: verb
        text:
          - 焼: や
          - き
          - 付: つ
          - ける
        dictionary: 焼付ける
      - pos: noun
        text:
          - 光: ひかり
      - pos: particle
        text: を
      - pos: noun
        text:
          - 背: せ
      - pos: particle
        text: に
      - pos: verb
        text:
          - 受: う
          - ける
  - - - dictionary: ~
        text: 「
      - pos: noun
        text:
          - 赤: あか
      - dictionary: ~
        text: 」
      - pos: particle
        text: に
      - pos: noun
        text:
          - 気: き
      - pos: particle
        text: を
      - pos: verb
        text:
          - 取: と
          - られている
        dictionary: 取る
english:
  - Tragic ends are shrouded in beauty
  - Endless, close to body heat 
  - Painted in red
  - I see its beauty through my blurred vision
  - Burning light on my back
  - I’m mesmerized by red
song:
  override: "[逆]{ぎゃっ}[光]{こう} *Backlight* from *One Piece Film: Red*"
youtube: gt-v_YCkaMY
translation:
  author: Mertosnake
  site:
    name: Lyrics Translate
    link: https://lyricstranslate.com/en/gyakkou-backlight.html
suggester: Communist_Mania
date: 2022-08-30
---

**Note:** If you’re wondering what the たる is in 惨憺たる, see [this Japanese Language Stack Exchange thread](https://japanese.stackexchange.com/questions/1013/what-exactly-is-a-taru-adjective). I hadn’t heard of ～たる adjectives until formatting this challenge
