---
japanese:
  - - - pos: adverb
        text:
          - 誰: だれ
          - でも
      - pos: adjective
        text: いい
        dictionary: 良い
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 嘘: うそ
      - pos: particle
        text: でも
      - pos: adjective
        text: いい
        dictionary: 良い
      - pos: particle
        text: から
      - pos: verb
        text:
          - 認: みと
          - めて
        dictionary: 認める
      - dictionary: ~
        text: ？
  - - - pos: noun
        text:
          - 最: さい
          - 後: ご
      - pos: particle
        text: の
      - pos: noun
        text:
          - 逃: に
          - げ
          - 場: ば
      - pos: verb
        text: だ
      - pos: particle
        text: から
      - pos: particle
        text: さ
  - - - pos: noun
        text:
          - 夢: ゆめ
      - pos: particle
        text: でも
      - pos: verb
        text:
          - 構: かま
          - わない
        dictionary: 構う
      - pos: particle
        text: から
  - - - pos: noun
        text:
          - 手: て
      - pos: verb
        text:
          - 繋: つな
          - いでい
        dictionary: 繋ぐ
      - pos: verb
        text: よう
      - pos: particle
        text: よ
english:
  - Please approve of me, whoever, even if it was a lie
  - ‘cause it is the last place to escape to
  - I don’t mind if it was only a dream
  - Let’s hold our hands together
song:
  japanese: アンダーキッズ
  english: Under Kids
youtube: TBoBfT-_sfM
date: 2022-07-27
---
