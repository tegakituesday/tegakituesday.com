---
japanese:
  - - - pos: noun
        text: ビーフステーキ
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 天: てん
          - ぷら
      - dictionary: ~
        text: "　"
      - pos: noun
        text: うなぎ
        dictionary: 鰻
  - - - pos: noun
        text: カルビ
      - dictionary: ~
        text: "　"
      - pos: noun
        text: じゃが
        dictionary: じゃが芋
      - pos: noun
        text: バター
      - dictionary: ~
        text: "　"
      - pos: noun
        text: ビーフストロガノフ
  - - - pos: noun
        text:
          - すき
          - 焼: や
          - き
      - dictionary: ~
        text: "　"
      - pos: noun
        text: シメパフェ
        dictionary: 締めパフェ
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 炭: すみ
          - 火: び
          - 焼: や
          - き
      - pos: noun
        text: ハンバーグ
english:
  - Beefsteak, tempura, eel,
  - galbi, buttered potatoes, beef stroganoff​,
  - sukiyaki, a post-meal parfait, and char-grilled hamburger steak.
song:
  japanese: ハッピーチートデー
  english: Happy Cheat Day
youtube: 7l-iaGDdrnw
translation:
  author: MadamePhoton
  site:
    name: Vocaloid Lyrics Wiki
    link: https://vocaloidlyrics.fandom.com/wiki/ハッピーチートデー_(Happy_Cheat_Day)
date: 2023-12-26
---