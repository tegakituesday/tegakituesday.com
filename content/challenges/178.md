---
japanese:
  - - - pos: noun
        text:
          - 色: いろ
      - pos: particle
        text: は
      - pos: verb
        text:
          - 匂: にお
          - えど
        dictionary: 匂う
      - dictionary: ~ 
        text: "　"
      - pos: verb
        text:
          - 散: ち
          - り
        dictionary: 散る
      - pos: verb
        text: ぬる
        dictionary: 塗る
      - pos: particle
        text: を
  - - - pos: adjective
        text:
          - 我: わ
          - が
      - pos: noun
        text:
          - 世: よ
      - pos: noun
        text:
          - 誰: たれ
      - dictionary: ~
        text: "　　　"
      - pos: noun
        text:
          - 常: つね
      - pos: verb
        text: ならん
        dictionary: ならない
  - - - pos: noun
        text:
          - 有: う
          - 為: い
      - pos: particle
        text: の
      - pos: noun
        text:
          - 奥: おく
          - 山: やま
      - dictionary: ~
        text: "　"
      - pos: adverb
        text:
          - 今日: きょう
      - pos: verb
        text:
          - 越: こ
          - えて
  - - - pos: adjective
        text:
          - 浅: あさ
          - き
        dictionary: 浅い
      - pos: noun
        text:
          - 夢: ゆめ
      - pos: verb
        text:
          - 見: み
        dictionary: 見る
      - pos: verb
        text: じ
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 酔: よ
          - い
      - pos: particle
        text: も
      - pos: phrase
        text: せず
  - - - dictionary: ~
        text: ""
  - - - dictionary: ~
        text: "いろはにほへと　ちりぬるを"
  - - - dictionary: ~
        text: "わかよたれそ　　つねならむ"
  - - - dictionary: ~
        text: "うゐのおくやま　けふこえて"
  - - - dictionary: ~
        text: "あさきゆめみし　ゑひもせす"
suggester: anachu
date: 2024-11-12
---

No skipping hiragana day for you

[Source](https://en.wikipedia.org/wiki/Iroha#Text)