---
japanese:
  - - - pos: noun
        text:
          - 孤: こ
          - 独: どく
      - pos: particle
        text: の
      - pos: noun
        text:
          - 月: つき
      - pos: particle
        text: が
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 闇: やみ
      - pos: particle
        text: を
      - pos: verb
        text:
          - 刺: さ
          - す
  - - - pos: adjective
        text:
          - 深: ふか
          - い
      - pos: adjective
        text:
          - 時: とき
          - の
      - pos: noun
        text:
          - 底: そこ
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 眠: ねむ
          - り
      - pos: phrase
        text: について
        dictionary: に就いて
      - pos: particle
        text: は
  - - - pos: adverb
        text:
          - 何: なん
          - 度: ど
          - も
      - pos: verb
        text:
          - 繰: く
          - り
          - 返: かえ
          - している
        dictionary: 繰り返す
  - - - pos: noun
        text:
          - 千: せん
      - pos: particle
        text: の
      - pos: noun
        text:
          - 記: き
          - 憶: おく
      - pos: verb
        text:
          - 辿: たど
          - る
      - pos: noun
        text:
          - 呪: じゅ
          - 文: もん
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 身: み
      - pos: particle
        text: に
      - pos: verb
        text:
          - 纏: まと
          - って
        dictionary: 纏う
song:
  english: Maze of the Dark
youtube: wjuRQG_AsrA
suggester: terviesreee
date: 2023-02-01
---
