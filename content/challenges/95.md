---
japanese:
  - - - pos: verb
        text:
          - 知: し
          - り
        dictionary: 知る
      - pos: verb
        text: たくない
        dictionary: たい
      - pos: noun
        text: こと
        dictionary: 事
      - pos: particle
        text: が
      - dictionary: ~
        text: "　"
      - pos: adverb
        text: あまりに
        dictionary: 余りに
      - pos: adjective
        text:
          - 多: おお
        dictionary: 多い
      - pos: verb
        text: すぎて
        dictionary: 過ぎる
  - - - pos: noun
        text:
          - 僕: ぼく
      - pos: particle
        text: は
      - pos: noun
        text: いつ
        dictionary: 何時
      - pos: particle
        text: から
      - pos: particle
        text: かね
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 両: りょう
          - 目: め
      - pos: particle
        text: を
      - pos: verb
        text:
          - 閉: と
          - じていた
        dictionary: 閉じる
  - - - pos: noun
        text:
          - 僕: ぼく
          - たち
        dictionary: 僕達
      - pos: particle
        text: は
      - pos: adverb
        text: いつでも
        dictionary: 何時でも
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 誰: だれ
          - か
      - pos: particle
        text: と
      - pos: verb
        text:
          - 比: ひ
          - 較: かく
      - pos: verb
        text: して
        dictionary: 為る
  - - - pos: phrase
        text:
          - その
          - 度: たび
      - pos: noun
        text:
          - 不: ふ
          - 幸: こう
      - pos: particle
        text: に
      - pos: verb
        text: なる
        dictionary: 成る
      - pos: noun
        text: こと
        dictionary: 事
      - pos: particle
        text: を
      - pos: verb
        text: やめない
        dictionary: 止める
      - pos: particle
        text: ね
embellishment: "(:birthday: 2 years!)"
song:
  japanese: "[優]{ゆう}[等]{とう}[生]{せい}"
youtube: lIZvDgFmSEk 
suggester: モカ妹紅（MochaMoko）
date: 2023-01-17
---

<p style="text-align: center"><small><a href="#challenge-content">⬇ Jump to challenge info</small></a></p>

{{< banner >}}**Thank you everyone for two years of Tegaki Tuesday!**{{< /banner >}}

It's hard to believe we've already been at this for [two years](/1), with 1,323 submission images totaling 2.0 GB across 94 challenges.

Thank you so much to everyone who has contributed to the challenge, and special thanks to **Ryry013** for originally starting the challenge back in 2017, the EJLX and 日英 mods for allowing 字ちゃん to be on their respective servers, **Steamedbread** and **Drago** for general support, **Nana** for translation help, **MochaMoko** for the never-ending stream of suggestions, **WrongWong** for providing the announcement banners in the early days, and of course **everyone who has submitted.** (I've probably forgotten someone, I'm half asleep.)

Here's to many more years to come! :black_nib: :beers:

<hr id="challenge-content">
