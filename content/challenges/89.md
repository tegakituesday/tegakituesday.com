---
japanese:
  - - - pos: phrase
        text: ねえ
        dictionary: ね
      - dictionary: ~
        text: "、"
      - pos: noun
        text: あい
        dictionary: 愛
      - pos: particle
        text: を
      - pos: verb
        text: さけぶ
        dictionary: 叫ぶ
      - pos: particle
        text: の
      - pos: particle
        text: なら
  - - - pos: noun
        text: あたし
        dictionary: 私-1
      - pos: particle
        text: は
      - pos: noun
        text: ここ
        dictionary: 此処
      - pos: particle
        text: に
      - pos: verb
        text: いる
        dictionary: 居る
      - pos: particle
        text: よ
  - - - pos: noun
        text: ことば
        dictionary: 言葉
      - pos: particle
        text: が
      - pos: verb
        text: ありあまれ
        dictionary: 有り余る
      - pos: particle
        text: ど
      - pos: adverb
        text: なお
        dictionary: 尚
      - dictionary: ~
        text: "、"
  - - - pos: adjective
        text: この
        dictionary: 此の
      - pos: noun
        text: ゆめ
        dictionary: 夢
      - pos: particle
        text: は
      - pos: verb
        text: つづいて
        dictionary: 続く
      - pos: verb
        text: く
        dictionary: 行く
  - - - pos: noun
        text: あたし
        dictionary: 私-1
      - pos: particle
        text: が
      - pos: noun
        text: あい
        dictionary: 愛
      - pos: particle
        text: を
      - pos: verb
        text: かたる
        dictionary: 語る
      - pos: particle
        text: の
      - pos: particle
        text: なら
  - - - pos: adjective
        text: その
        dictionary: 其の
      - pos: noun
        text: すべて
        dictionary: 全て
      - pos: particle
        text: は
      - pos: adjective
        text: この
        dictionary: 此の
      - pos: noun
        text: うた
        dictionary: 歌
      - pos: verb
        text: だ
  - - - pos: noun
        text: だれも
        dictionary: 誰も
      - pos: verb
        text: しらない
        dictionary: 知る
      - pos: adjective
        text: この
        dictionary: 此の
      - pos: noun
        text: ものがたり
        dictionary: 物語
  - - - pos: adverb
        text: また
        dictionary: 又
      - pos: verb
        text: くちずさんで
        dictionary: 口ずさむ
      - pos: verb
        text: しまった
        dictionary: 仕舞う
      - pos: adjective
        text: みたい
      - pos: verb
        text: だ
english:
  - Hey, if you’re gonna scream about your love
  - I’ll be right here
  - Words may be excessive, but,
  - This dream continues on
  - If I was to express my love
  - That everything is this song.
  - That story unknown to all,
  - It seems I’ve gone and hummed it once again.
song:
  japanese: アンノウン・マザーグース
  english: Unknown Mother-Goose
youtube: P_CSdxSGfaA
translation:
  author: ForgetfulSubs
  site:
    name: Vocaloid Lyrics Wiki
    link: https://vocaloidlyrics.fandom.com/wiki/アンノウン・マザーグース_(Unknown_Mother_Goose)
suggester: yuunagi
date: 2022-12-13
---
