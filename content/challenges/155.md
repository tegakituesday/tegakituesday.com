---
japanese:
  - - - pos: noun
        text: パイロマニア
        dictionary: 放火狂
      - pos: particle
        text: の
      - pos: verb
        text:
          - 悲: かな
          - しい
      - pos: noun
        text:
          - 世: せ
          - 界: かい
  - - - pos: verb
        text:
          - 燃: も
          - える
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 燃: も
          - える
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 炎: ほのお
      - pos: particle
        text: が
  - - - pos: verb
        text:
          - 揺: ゆ
          - れる
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 揺: ゆ
          - れる
  - - - pos: adverb
        text:
          - もう
          - 一: いっ
          - 回: かい
      - pos: verb
        text:
          - 繰: く
          - り
          - 返: かえ
        dictionary: 繰り返す
      - pos: verb
        text: そう
        dictionary: よう
      - pos: particle
        text: よ
  - - - pos: adjective
        text:
          - 長: なが
          - い
      - pos: noun
        text:
          - 影: かげ
      - pos: particle
        text: を
      - pos: verb
        text: かけて
        dictionary: 掛ける
      - pos: verb
        text:
          - 消: き
          - え
        dictionary: 消える
      - pos: verb
        text: たい
  - - - pos: verb
        text:
          - 溶: と
          - ける
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 溶: と
          - ける
      - pos: particle
        text: よ
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 痛: いた
          - い
  - - - pos: noun
        text:
          - 僕: ぼく
      - pos: particle
        text: は
      - pos: noun
        text:
          - 誰: だれ
      - pos: phrase
        text: なの
      - dictionary: ~
        text: "？"
      - pos: verb
        text:
          - 忘: わす
          - れて
        dictionary: 忘れる
      - pos: verb
        text: いく
        dictionary: 行く
english:
  - The sad world of pyromania
  - A burning, burning flame
  - It sways, it sways
  - Let's repeat it again
  - I want to disappear casting a long shadow
  - I'm melting, melting, it hurts
  - I'm forgetting who I really am
song:
  japanese: パイロマニア
date: 2024-04-11
youtube: F4REaTQgcXs
---