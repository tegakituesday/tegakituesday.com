---
japanese:
  - - - pos: adjective
        text:
          - 大: だい
          - 事: じ
          - な
        dictionary: 大事
      - pos: noun
        text:
          - 夢: ゆめ
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 諦: あきら
          - めない
        dictionary: 諦める
      - pos: particle
        text: から
  - - - pos: noun
        text:
          - 少: すこ
          - し
      - pos: particle
        text: だけ
        dictionary: 丈
      - pos: noun
        text:
          - 勇: ゆう
          - 気: き
      - dictionary: ~
        text: "　"
      - pos: verb
        text:
          - 分: わ
          - けて
        dictionary: 分ける
      - pos: verb
        text: ほしい
        dictionary: 欲しい
      - pos: particle
        text: よ
  - - - pos: verb
        text: しまっていた
        dictionary: 仕舞う
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 心: こころ
      - pos: particle
        text: の
      - pos: noun
        text:
          - 扉: とびら
      - pos: verb
        text:
          - 開: あ
          - けて
  - - - dictionary: 開ける
        pos: adjective
        text:
          - 素: す
          - 敵: てき
          - な
        dictionary: 素敵
      - pos: noun
        text:
          - 歌: うた
      - pos: particle
        text: を
      - dictionary: ~
        text: "　"
      - pos: adverb
        text: ずっと
      - dictionary: ~
        text: "　"
      - pos: noun
        text:
          - 届: とど
          - けられる
        dictionary: 届く
      - pos: particle
        text: よ
song:
  japanese: "[今]{こん}[夜]{や}だけは～"
  english: Just for tonight～
youtube: fPjsGl9Cnak
suggester: mochamoko
date: 2023-06-20
---